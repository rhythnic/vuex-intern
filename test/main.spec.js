const {
	//getters
	findByKey,
	filterByKey,
	//mutations
	adjustListIndex,
	assignConstant,
	extendRecordInList,
	pushTo,
	replaceRecordInList,
	set,
	toggle,
	without
} = require('../dist')

describe('vuex-intern', () => {
	const users = [
		{ id: 1, status: 'ACTIVE' },
		{ id: 2, status: 'ACTIVE' },
		{ id: 3, status: 'INACTIVE' }
	]

	const tags = ['a', 'b', 'c']

	describe('getters', () => {
		describe('findByKey', () => {
			const state = { users, org: { users } }
			it('returns one list item if given a single value', () => {
				let find = findByKey('users', 'id')
				expect(find(state)(1)).toBe(state.users[0])
				find = findByKey(['org', 'users'], 'id')
				expect(find(state)(1)).toBe(state.org.users[0])
			})
			it('returns a list if given a list of values', () => {
				let find = findByKey('users', 'id')
				expect(find(state)([1, 2])).toEqual(state.users.slice(0, -1))
				find = findByKey(['org', 'users'], 'id')
				expect(find(state)([1, 2])).toEqual(state.org.users.slice(0, -1))
			})
		})
		describe('filterByKey', () => {
			it('filters a list by a key/value pair', () => {
				const state = { users, org: { users } }
				expect(filterByKey('users', 'status')(state)('ACTIVE')).toEqual(state.users.slice(0, 2))
				expect(filterByKey(['org', 'users'], 'status')(state)('INACTIVE')).toEqual(state.org.users.slice(2))
			})
		})
	})

	describe('mutations', () => {
		describe('adjustListIndex', () => {
			it('adjusts an list index in state when provided a list', () => {
				const state = { userIndex: 0 }
				adjustListIndex('userIndex', users)(state, 1)
				expect(state.userIndex).toBe(1)
			})
			it('adjusts an list index in state when provided a key to the list in state', () => {
				const state = { userIndex: 0, users }
				adjustListIndex('userIndex', 'users')(state, 1)
				expect(state.userIndex).toBe(1)
			})
			it('modulos the index by the list length', () => {
				const state = { userIndex: 0 }
				adjustListIndex('userIndex', users)(state, 3)
				expect(state.userIndex).toBe(0)
			})
			it('converts a negative index to positive by counting from the end of the list', () => {
				const state = { userIndex: 0 }
				adjustListIndex('userIndex', users)(state, -1)
				expect(state.userIndex).toBe(2)
			})
		})
		describe('assignConstant', () => {
			it('merges one object into state', () => {
				const data = { a: 0 }
				const state = {}
				assignConstant(data)(state)
				expect(state).toMatchObject(data)
			})
		})
		describe('extendRecordInList', () => {
			const extend = extendRecordInList('users', 'id')
			it('it adds a record to the list if not found', () => {
				const state = { users: [...users] }
				const user = { id: 4 }
				extend(state, user)
				expect(state.users).toContain(user)
			})
			it('it extends a record if found', () => {
				const state = { users: [...users] }
				const user = { id: 2, status: 'INACTIVE' }
				extend(state, user)
				expect(state.users[1]).toHaveProperty('status', 'INACTIVE')
			})
		})
		describe('pushTo', () => {
			it('pushes an item onto an array', () => {
				const state = { users: [...users] }
				const user = { id: 4 }
				pushTo('users')(state, user)
				expect(state.users[3]).toBe(user)
			})
		})
		describe('replaceRecordInList', () => {
			const replace = replaceRecordInList('users', 'id')
			it('it adds a record to the list if not found', () => {
				const state = { users: [...users] }
				const user = { id: 4 }
				replace(state, user)
				expect(state.users).toContain(user)
			})
			it('it replaces a record if found', () => {
				const state = { users: [...users] }
				const user = { id: 2, status: 'INACTIVE' }
				replace(state, user)
				expect(state.users[1]).toHaveProperty('status', 'INACTIVE')
			})
		})
		describe('set', () => {
			it('sets the value of a property on state', () => {
				const state = {}
				set('a')(state, 0)
				expect(state.a).toBe(0)
			})
			it('sets the value to a path within state', () => {
				const state = { a: [{ b: 0 } ] }
				set(['a', 0, 'b'])(state, 1)
				expect(state.a[0].b).toBe(1)
			})
			it('creates objects along the set path as necessary', () => {
				const state = {}
				set(['a', 0, 'b'])(state, 1)
				expect(state.a[0].b).toBe(1)
			})
		})
		describe('toggle', () => {
			it('toggles a boolean in state', () => {
				const state = { bool: true }
				toggle('bool')(state)
				expect(state.bool).toBe(false)
				toggle('bool')(state)
				expect(state.bool).toBe(true)
			})
		})
		describe('without', () => {
			it('removes items from a list in state', () => {
				const state = { tags: [...tags] }
				without('tags')(state, tags.slice(0, 2))
				expect(state.tags).toHaveLength(1)
				expect(state.tags[0]).toBe('c')
			})
			it('removes records from a list in state', () => {
				const state = { users: [...users] }
				without('users', 'id')(state, 1)
				expect(state.users).toHaveLength(2)
				expect(state.users[0].id).toBe(2)
			})
		})
	})
})
