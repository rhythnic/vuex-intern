# vuex-intern

`vuex-intern` is a set of functions that facilitate common types of mutations and getters in
[Vuex](https://vuex.vuejs.org/)

## Install

`npm install vuex-intern`

## Docs

Here is the state for all examples.

```
const state = {
	users: [
		{ id: 1, status: 'ACTIVE' },
		{ id: 2, status: 'ACTIVE' },
		{ id: 3, status: 'INACTIVE' }
	],
	userIndex: 0,
	open: false,
	authorizedUser: null,
	tags: ['book', 'movie', 'walking tour']
}
```

### Getters

#### findByKey
`findByKey` finds an object within a list by searching for a key/value pair.  If passed a single value,
it will return a single object (if found).  If passed an array, it will return an array.
The first argument ('users' in the example below) can be an array if the property is nested in state.


```
import { findByKey } from 'vuex-intern'

// configure
const getters = {
	userById: findByKey('users', 'id')
}

// use
const user = store.getters.userById(1)
const users = store.getters.userById([1, 2, 3])
```

#### filterByKey
`filterByKey` filters a list against a key/value pair.  `filterByKey` accepts a single value or an array of potential values.
The first argument ('users' in the example below) can be an array if the property is nested in state.

```
import { filterByKey } from 'vuex-intern'

// configure
const getters = {
	usersByStatus: filterByKey('users', 'status')
}

// use
const activeUsers = store.getters.usersByStatus('ACTIVE')
const inactiveUsers = store.getters.usersByStatus(['INACTIVE', 'SUSPENDED'])
```

### Mutations

#### adjustListIndex
`adjustListIndex` will adjust an array index in state by the provided value.
Use a positive value to move forward in the index or a negative value to move
backwards.  For the list, you can pass a key to a list in state ("users" in the
example below), or you can pass an actual array.

```
import { adjustListIndex } from 'vuex-intern'

// configure
const mutations = {
	moveUserIndex: adjustListIndex('userIndex', 'users')
}

// use
store.commit('moveUserIndex', 1)
// userIndex is now 1
store.commit('moveUserIndex', -2)
// userIndex is now 2
```

#### assignConstant
`assignConstant` will merge the same object into state, regardless of the value passed to the commit call.
It's useful for resetting parts of state to initial values.

```
import { assignConstant } from 'vuex-intern'

// configure
const mutations = {
	reset: assignConstant(initialState)
}

// use
store.commit('reset')
```

#### extendRecordInList
`extendRecordInList` will add a record to a list or merge the new values into the existing record.

```
import { extendRecordInList } from 'vuex-intern'

// configure
const mutations = {
	addUser: extendRecordInList('users', 'id')
}

// use
// add record
store.commit('addUser', { id: 4, status: 'ACTIVE' })
// extend record
store.commit('addUser', { id: 4, username: 'jez' })
// record is now { id: 4, status: 'ACTIVE', username: 'jez' }
```

#### replaceRecordInList
`replaceRecordInList` will add a record to a list or replace the record if it exists in the list.

```
import { replaceRecordInList } from 'vuex-intern'

// configure
const mutations = {
	addUser: replaceRecordInList('users', 'id')
}

// use
// add record
store.commit('addUser', { id: 4, status: 'ACTIVE' })
// extend record
store.commit('addUser', { id: 4, username: 'jez' })
// record is now { id: 4, username: 'jez' }
```

#### set
`set` is an abstraction over `setProp` and `setPath`.

```
import { set } from 'vuex-intern'

const mutations = {
	setAuthorizedUser: set('authorizedUser'), // uses setProp
	setAuthToken: set(['authorizedUser', 'token']) // uses setPath
}

// use
store.commit('setAuthorizedUser', { token: 'abc', uid: '1' })
store.commit('setAuthToken', 'cbd')
```

#### setPath
`setPath` sets a value at a path within state.  It creates the objects and arrays
within the path if they don't exist.  An integer in the path will cause an
array to be created.  A string in the path will cause an object to be created, even
if it's "0".

```
import { setPath } from 'vuex-intern'

const mutations = {
	setAuthToken: setPath(['authorizedUser', 'token'])
}

// use
store.commit('setAuthToken', 'cbd')
```

#### setProp
`setProp` sets a value in state

```
import { setProp } from 'vuex-intern'

const mutations = {
	setAuthorizedUser: setProp('authorizedUser')
}

// use
store.commit('setAuthorizedUser', { token: 'abc', uid: '1' })
```

#### toggle
`toggle` toggles a boolean value.

```
import { toggle } from 'vuex-intern'

const mutations = {
	toggleOpen: toggle('open')
}

// use
store.commit('toggleOpen')
```

#### without
`without` removes items from a list in state.  without can remove simple items, like strings, or
it can remove records, if configured with a "targetKey" like "id" below.

```
import { without } from 'vuex-intern'

const mutations = {
	removeUsers: without('users', 'id')
	removeTags: without('tags')
}

// use
store.commit('removeTags', ['movie', 'walking tour'])
// tags is now ['book']
store.commit('removeUsers', [1, 2])
// users is now [{ id: 3, status: 'INACTIVE' }]
```
